# Example
Use example below to configure tour device

```
mikrotik_interfaces:
  reset_ethernet_interfaces: false
  delete_all_vlan_interfaces: false

  delete_interfaces_on_target_router: true

  ethernets:
    - comment: (string) # Required. Descriptive name of an item
      default-name: (string) # Required. Default (hardware) name of an interface - used for matching.
      disabled: (yes|no) # Required. if true - interface is disabled.
      name: (string) # Default: None. Name of an interface.
      advertise: (string) # Default: "10M-half,10M-full,100M-half,100M-full,1000M-full". List of ethernet modes to advertise, separated by comma.
      arp: (disabled | enabled | proxy-arp | reply-only) # Default: enabled. Address Resolution Protocol mode: disabled - the interface will not use ARP; enabled - the interface will use ARP; proxy-arp - the interface will use the ARP proxy feature; reply-only - the interface will only reply to requests originated from matching IP address/MAC address combinations which are entered as static entries in the ARP table. No dynamic entries will be automatically stored in the ARP table. Therefore for communications to be successful, a valid static entry must already exist.
      arp-timeout: (auto|time) # Default: auto. ARP timeout.
      auto-negotiation: (yes | no) # Default: yes. When enabled, the interface "advertises" its maximum capabilities to achieve the best connection possible. Note1: Auto-negotiation should not be disabled on one end only, otherwise Ethernet Interfaces may not work properly. Note2: Gigabit link cannot work with auto-negotiation disabled.
      tx-flow-control: (on | off | auto) # Default: off. When set to on, port will send pause frames when specific buffer usage thresholds is met. auto is the same as on except when auto-negotiation=yes flow control status is resolved by taking into account what other end advertises. Feature is supported on AR724x, AR9xxx, QCA9xxx CPU ports, all CCR ports and all Atheros switch chip ports.
      rx-flow-control: (on | off | auto) # Default: off. When set to on, port will process received pause frames and suspend transmission if required. auto is the same as on except when auto-negotiation=yes flow control status is resolved by taking into account what other end advertises. Feature is supported on AR724x, AR9xxx, QCA9xxx CPU ports, all CCR ports and all Atheros switch chip ports.
      full-duplex: (yes | no) # Default: yes. Defines whether the transmission of data appears in two directions simultaneously
      # Not implemented. mac-address: (MAC) # Default: None. Media Access Control number of an interface.
      mdix-enable: (yes | no) # Default: yes. Whether the MDI/X auto cross over cable correction feature is enabled for the port: (Hardware specific, e.g. ether1 on RB500 can be set to yes/no. Fixed to 'yes' on other hardware.)
      mtu: (integer [0..65536]) # Default: 1500. Layer3 Maximum transmission unit
      # Not implemented. orig-mac-address: (MAC) # Default: None. Not documented.
      speed: (10Mbps | 10Gbps | 100Mbps | 1Gbps) # Default: None. Sets interface data transmission speed which takes effect only when auto-negotiation is disabled.
      loop-protect: (yes|no|default) # Default: default. Sets loop protection status.
      loop-protect-disable-time: (time) # Default: 5m. Sets time for which the interface will be disabled on loop detection
      loop-protect-send-interval: (time) # Default: 5s. Sets interval between loop checks.
  vlans:
    - name: (string) # Required. Interface name
      comment: (string) # Required. Descriptive comment of interface
      disabled: (yes|no) # Required. if true - interface is disabled.
      interface: (name) # Required. Name of physical interface on top of which VLAN will work
      vlan-id: (integer: 1..4095) # Default: 1. Virtual LAN identifier or tag that is used to distinguish VLANs. Must be equal for all computers that belong to the same VLAN.
      arp: (disabled | enabled | proxy-arp | reply-only) # Default: enabled. Address Resolution Protocol mode
      arp-timeout: (auto|time) # Default: auto. ARP protocol timeout.
      mtu: (integer) # Default: 1500. Layer3 Maximum transmission unit
      use-service-tag: (yes | no) # Default: no. 802.1ad compatible Service Tag
  bridges:
    - name: (string) # Required. Unique name of a bridge interface
      comment: (string) # Required. Descriptive name of the bridge interface
      member-ports:
        - interface: (string) # Required. Name of physical or logical interface to add to the bridge
          comment: (string) # Required. Description of the member interface
```
